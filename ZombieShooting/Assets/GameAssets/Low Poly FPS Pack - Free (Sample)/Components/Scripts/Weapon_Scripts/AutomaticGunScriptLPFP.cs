﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// ----- Low Poly FPS Pack Free Version -----
public class AutomaticGunScriptLPFP : BaseAction
{
    protected override int GetCurrentAmmo()
    {
        return GameManager.Instance._data.TotalBulletsInMachineGun;
    }
    protected override int GetTotalBulletNotUsing()
    {
        return GameManager.Instance._data.TotalMachineGunBullets;
    }

    protected override void SetCurrentAmmo(int ammo)
    {
        GameManager.Instance._data.TotalBulletsInMachineGun = ammo;
    }

    protected override void SetTotalBulletNotUsing(int bullet)
    {
        GameManager.Instance._data.TotalMachineGunBullets = bullet;
    }

    protected override void ShootBullet()
    {
        //AUtomatic fire
        //Left click hold 
        if (Input.GetMouseButton(0) && !outOfAmmo && !isReloading && !isInspecting && !isRunning)
        {
            //Shoot automatic
            if (Time.time - lastFired > 1 / fireRate)
            {
                lastFired = Time.time;

                //Remove 1 bullet from ammo
                SetCurrentAmmo(GetCurrentAmmo() - 1);

                shootAudioSource.clip = SoundClips.shootSound;
                shootAudioSource.Play();

                if (!isAiming) //if not aiming
                {
                    anim.Play("Fire", 0, 0f);
                    //If random muzzle is false
                    if (!randomMuzzleflash &&
                        enableMuzzleflash == true)
                    {
                        muzzleParticles.Emit(1);
                        //Light flash start
                        StartCoroutine(MuzzleFlashLight());
                    }
                    else if (randomMuzzleflash == true)
                    {
                        //Only emit if random value is 1
                        if (randomMuzzleflashValue == 1)
                        {
                            if (enableSparks == true)
                            {
                                //Emit random amount of spark particles
                                sparkParticles.Emit(Random.Range(minSparkEmission, maxSparkEmission));
                            }
                            if (enableMuzzleflash == true)
                            {
                                muzzleParticles.Emit(1);
                                //Light flash start
                                StartCoroutine(MuzzleFlashLight());
                            }
                        }
                    }
                }
                else //if aiming
                {

                    anim.Play("Aim Fire", 0, 0f);

                    //If random muzzle is false
                    if (!randomMuzzleflash)
                    {
                        muzzleParticles.Emit(1);
                        //If random muzzle is true
                    }
                    else if (randomMuzzleflash == true)
                    {
                        //Only emit if random value is 1
                        if (randomMuzzleflashValue == 1)
                        {
                            if (enableSparks == true)
                            {
                                //Emit random amount of spark particles
                                sparkParticles.Emit(Random.Range(minSparkEmission, maxSparkEmission));
                            }
                            if (enableMuzzleflash == true)
                            {
                                muzzleParticles.Emit(1);
                                //Light flash start
                                StartCoroutine(MuzzleFlashLight());
                            }
                        }
                    }
                }

                //Spawn bullet from bullet spawnpoint
                var bulletPrefab = ObjectPool.Instance.SpawnObject(_bulletPrefabKey);
                bulletPrefab.transform.SetPositionAndRotation(Spawnpoints.bulletSpawnPoint.transform.position,
                                                                Spawnpoints.bulletSpawnPoint.transform.rotation);
                bulletPrefab.SetActive(true);


                //Add velocity to the bullet
                bulletPrefab.GetComponent<Rigidbody>().velocity =
                    bulletPrefab.transform.forward * bulletForce;
                ChechTarget();

                //Spawn casing prefab at spawnpoint
                var casingPrefab = ObjectPool.Instance.SpawnObject(_casingPrefabKey);
                casingPrefab.SetActive(true);
                casingPrefab.transform.SetPositionAndRotation(Spawnpoints.casingSpawnPoint.transform.position,
                                                                Spawnpoints.casingSpawnPoint.transform.rotation);
            }
        }
    }
}