﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// ----- Low Poly FPS Pack Free Version -----
public class HandgunScriptLPFP : BaseAction
{
    [Header("Weapon Settings")]

    public float sliderBackTimer = 1.58f;
    private bool hasStartedSliderBack;

    protected override int GetCurrentAmmo()
    {
        return GameManager.Instance._data.TotalBulletsInPistolGun;
    }

    protected override int GetTotalBulletNotUsing()
    {
        return GameManager.Instance._data.TotalPistolGunBullets;
    }


    protected override void SetCurrentAmmo(int ammo)
    {
        GameManager.Instance._data.TotalBulletsInPistolGun = ammo;
    }

    protected override void SetTotalBulletNotUsing(int bullet)
    {
        GameManager.Instance._data.TotalPistolGunBullets = bullet;
    }

    protected override void Reload()
    {
        base.Reload();
        if (!hasStartedSliderBack)
        {
            hasStartedSliderBack = true;
            StartCoroutine(HandgunSliderBackDelay());
        }
    }

    private IEnumerator HandgunSliderBackDelay()
    {
        //Wait set amount of time
        yield return new WaitForSeconds(sliderBackTimer);
        //Set slider back
        anim.SetBool("Out Of Ammo Slider", false);
        //Increase layer weight for blending to slider back pose
        anim.SetLayerWeight(1, 0.0f);

        hasStartedSliderBack = false;
    }

    protected override void ShootBullet()
    {
        if (Input.GetMouseButtonDown(0) && !outOfAmmo && !isReloading && !isInspecting && !isRunning)
        {
            anim.Play("Fire", 0, 0f);

            muzzleParticles.Emit(1);

            //Remove 1 bullet from ammo
            SetCurrentAmmo(GetCurrentAmmo() - 1);

            shootAudioSource.clip = SoundClips.shootSound;
            shootAudioSource.Play();

            //Light flash start
            StartCoroutine(MuzzleFlashLight());

            if (!isAiming) //if not aiming
            {
                anim.Play("Fire", 0, 0f);

                muzzleParticles.Emit(1);

                if (enableSparks == true)
                {
                    //Emit random amount of spark particles
                    sparkParticles.Emit(Random.Range(1, 6));
                }
            }
            else //if aiming
            {
                anim.Play("Aim Fire", 0, 0f);

                //If random muzzle is false
                if (!randomMuzzleflash)
                {
                    muzzleParticles.Emit(1);
                    //If random muzzle is true
                }
                else if (randomMuzzleflash == true)
                {
                    //Only emit if random value is 1
                    if (randomMuzzleflashValue == 1)
                    {
                        if (enableSparks == true)
                        {
                            //Emit random amount of spark particles
                            sparkParticles.Emit(Random.Range(1, 6));
                        }
                        if (enableMuzzleflash == true)
                        {
                            muzzleParticles.Emit(1);
                            //Light flash start
                            StartCoroutine(MuzzleFlashLight());
                        }
                    }
                }
            }

            //Spawn bullet from bullet spawnpoint
            var bulletPrefab = ObjectPool.Instance.SpawnObject(_bulletPrefabKey);
            bulletPrefab.transform.SetPositionAndRotation(Spawnpoints.bulletSpawnPoint.transform.position,
                                                          Spawnpoints.bulletSpawnPoint.transform.rotation);
            bulletPrefab.SetActive(true);


            //Add velocity to the bullet
            bulletPrefab.GetComponent<Rigidbody>().velocity =
                bulletPrefab.transform.forward * bulletForce;
            ChechTarget();

            //Spawn casing prefab at spawnpoint
            var casingPrefab = ObjectPool.Instance.SpawnObject(_casingPrefabKey);
            casingPrefab.SetActive(true);
            casingPrefab.transform.SetPositionAndRotation(Spawnpoints.casingSpawnPoint.transform.position,
                                                            Spawnpoints.casingSpawnPoint.transform.rotation);
        }

    }
}