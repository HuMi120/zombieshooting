using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadingManager : MonoBehaviour
{
    public enum SceneLoadingTag
    {
        INTROL,
        LOBBY,
        GAMEPLAY
    }
    [System.Serializable]
    public class SceneLoadingName
    {
        public SceneLoadingTag _tag;
        public string _sceneName;
    }
    [SerializeField] private SceneLoadingName[] _listScene;
    private Action<float> _callBackLoadingScene;
    private Action _callBackStartLoading;
    private Action _callBackFinishLoading;
    public void InitActionOnLoading(Action<float> callBackLoadingScene = null, Action callBackStartLoading = null, Action callBackFinishLoading = null)
    {
        _callBackLoadingScene += callBackLoadingScene;
        _callBackStartLoading += callBackStartLoading;
        _callBackFinishLoading += callBackFinishLoading;
    }
    private bool _isLoadingScene = false;
    public void LoadScene(SceneLoadingTag sceneTag)
    {
        if (_isLoadingScene) return;
        _isLoadingScene = true;
        for (int i = 0, length = _listScene.Length; i < length; i++)
        {
            if (_listScene[i]._tag == sceneTag)
            {
                StartCoroutine(LoadAsyncScene(_listScene[i]._sceneName));
            }
        }
    }
    private IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.completed += (AsyncOperation) => { _callBackFinishLoading?.Invoke(); };
        _callBackStartLoading?.Invoke();
        while (!asyncLoad.isDone)
        {
            if (asyncLoad.progress <= 0.85f)
            {
                _callBackLoadingScene?.Invoke(asyncLoad.progress);
                yield return null;
            }
            else
            {
                _callBackLoadingScene?.Invoke(asyncLoad.progress);
                yield return new WaitForSeconds(0.25f);
            }

        }
        _isLoadingScene = false;
    }
}
