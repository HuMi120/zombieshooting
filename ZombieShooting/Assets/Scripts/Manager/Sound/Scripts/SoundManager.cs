﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public void ChangeBackgroundVolume(float volume) => _backgroundAudioSource.volume = volume * _baseMaxBackgroundMusicVolume;
    public void ChangeEffectSoundVolume(float volume)
    {
        for (int i = 0, length = _soundEffectAudioSources.Count; i < length; i++)
        {
            _soundEffectAudioSources[i].volume = volume * _baseMaxSoundEffectVolume;
        }
        _soundEffectOnlyOneAuidoSources.volume = volume * _baseMaxSoundEffectVolume;
    }
    [SerializeField] private Transform _soundEffectParent;
    [SerializeField] private AudioSource _backgroundAudioSource;
    [SerializeField] private AudioSource _soundEffectOnlyOneAuidoSources;
    [SerializeField] private Transform _soundEffectOnlyOneAudioSourcesTransform;
    [SerializeField] private SoundLibrary _soundLibrary;
    [SerializeField] private float _baseMaxSoundEffectVolume = 1;
    [SerializeField] private float _baseMaxBackgroundMusicVolume = 1;
    private List<AudioSource> _soundEffectAudioSources = new List<AudioSource>();
    public void PlayBackgroudnSound(int backgroundSoundID)
    {
        if (_backgroundAudioSource.isPlaying) _backgroundAudioSource.Stop();
        _backgroundAudioSource.clip = _soundLibrary.GetBackgroundSoundByID(backgroundSoundID);
        _backgroundAudioSource.Play();
    }
    public void StopBackgroundSound()
    {
        if (_backgroundAudioSource.isPlaying) _backgroundAudioSource.Stop();
    }
    public void PlayEffectSoundOnlyOne(SoundLibrary.SoundEffectName soundEffectName, bool isLoop = false, Transform parent = null)
    {
        _soundEffectOnlyOneAudioSourcesTransform.parent = parent ?? _soundEffectParent;
        if (_soundEffectOnlyOneAuidoSources.isPlaying) _soundEffectOnlyOneAuidoSources.Stop();
        _soundEffectOnlyOneAuidoSources.clip = _soundLibrary.GetEffectSoundByName(soundEffectName);
        _soundEffectOnlyOneAuidoSources.loop = isLoop;
        _soundEffectOnlyOneAuidoSources.Play();
    }
    public void StopEffectSoundOnlyOne()
    {
        if (_soundEffectOnlyOneAuidoSources.isPlaying) _soundEffectOnlyOneAuidoSources.Stop();
    }
    public void PlayEffectSound(SoundLibrary.SoundEffectName soundEffectName, bool isLoop = false, Transform parent = null)
    {
        AudioSource _tempAuidoSource = null;
        for (int i = 0, length = _soundEffectAudioSources.Count; i < length; i++)
        {
            if (!_soundEffectAudioSources[i].isPlaying) _tempAuidoSource = _soundEffectAudioSources[i];
        }
        if (_tempAuidoSource == null)
        {
            GameObject go = new GameObject("Sound Effect");
            go.AddComponent(typeof(AudioSource));
            go.transform.parent = parent ?? _soundEffectParent;
            _tempAuidoSource = go.GetComponent<AudioSource>();
            _tempAuidoSource.playOnAwake = false;
            _tempAuidoSource.loop = isLoop;
            _tempAuidoSource.volume = _soundEffectOnlyOneAuidoSources.volume;
            _soundEffectAudioSources.Add(_tempAuidoSource);
        }
        else
        {
            _tempAuidoSource.transform.parent = parent ?? _soundEffectParent;
            _tempAuidoSource.loop = isLoop;
        }
        _tempAuidoSource.clip = _soundLibrary.GetEffectSoundByName(soundEffectName);
        _tempAuidoSource.Play();
    }
    public void StopEffectSounds()
    {
        for (int i = 0, length = _soundEffectAudioSources.Count; i < length; i++)
        {
            if (_soundEffectAudioSources[i].isPlaying) _soundEffectAudioSources[i].Stop();
        }
        if (_soundEffectOnlyOneAuidoSources.isPlaying) _soundEffectOnlyOneAuidoSources.Stop();
    }
    public void StopAllEffectSounds()
    {
        StopEffectSoundOnlyOne();
        StopEffectSounds();
    }
    public void ActionVibrate()
    {
        //if (GameManager.Instance._data.AllowVibrate) Handheld.Vibrate();
    }
}
