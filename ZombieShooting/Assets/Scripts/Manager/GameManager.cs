﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState
{
    Init,
    GamePlay
}
public class GameManager : SingletonOneScene<GameManager>
{
    public GamePlayUIControl _ui;
    public PlayerController _playerController;
    [HideInInspector] public DataBase _data;
    private GameState _gameState = GameState.Init;

    private void Awake()
    {
        StartCoroutine(Init());
    }
    public override void OnDestroy()
    {
        _data._callBackOnUpdateAmmo -= _ui.SetPlayerAmmoShowTxt;
        _playerController._callBackOnSwitchWeapon -= PlayerChangeWeapon;
        base.OnDestroy();
    }
    private IEnumerator Init()
    {
        _playerController.DisableControl();
        _data = FindObjectOfType<DataBase>();
        _data._callBackOnUpdateAmmo += _ui.SetPlayerAmmoShowTxt;
        _playerController._callBackOnSwitchWeapon += PlayerChangeWeapon;
        _playerController._callBackHeartDecress += _ui.SetPlayerHeart;
        _playerController._callBackHeartDecress += _ui.StartBloodEffectUI;
        PlayerChangeWeapon(true);
        yield return null;
        _playerController.EnableControl();
        _ui.SetPlayerHeart(_playerController.GetPlayerHeart());
    }
    private void PlayerChangeWeapon(bool isUsingMachineGun)
    {
        if (isUsingMachineGun)
        {
            _ui.SetPlayerWeaponName("Machine Gun");
            _ui.SetPlayerAmmoShowTxt(GameManager.Instance._data.TotalBulletsInMachineGun,
                                    GameManager.Instance._data.TotalMachineGunBullets);
        }
        else
        {
            _ui.SetPlayerWeaponName("Pistol");
            _ui.SetPlayerAmmoShowTxt(GameManager.Instance._data.TotalBulletsInPistolGun,
                                    GameManager.Instance._data.TotalPistolGunBullets);
        }
    }
}
