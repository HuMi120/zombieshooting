﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayUIControl : MonoBehaviour
{
    [Header("View")]
    [SerializeField] private GamePlayUIView _view;
    public void SetPlayerAmmoShowTxt(int ammoInGun, int ammoNotUsing)
    {
        _view.SetPlayerAmmoShowTxt(ammoInGun, ammoNotUsing);
    }
    public void SetPlayerWeaponName(string weaponName)
    {
        _view.SetPlayerWeaponName(weaponName);
    }
    public void SetPlayerHeart(int playerHeart)
    {
        _view.SetPlayerHeart(playerHeart);
    }
    public void StartBloodEffectUI(int playerHeart)
    {
        _view.StartBloodEffect();
    }
}
