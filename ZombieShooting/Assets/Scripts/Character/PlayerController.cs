﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FPSControllerLPFP;
using System;

public class PlayerController : MonoBehaviour
{
    [Header("Control")]
    [SerializeField] private FpsControllerLPFP _fpsController;
    [SerializeField] private AutomaticGunScriptLPFP _machineGunController;
    [SerializeField] private HandgunScriptLPFP _pistolGunController;
    [Header("Type of Weapon")]
    [SerializeField] private GameObject _machineGunArm;
    [SerializeField] private GameObject _pistolGunArm;
    [Header("Holstered")]
    [SerializeField] private float _machineGunHolsteredTime = 1f;
    [SerializeField] private float _pistolGunHolsteredTime = 1f;
    private bool _isUsingMachineGun = true;
    [HideInInspector] public bool _isChangeGun = false;
    public Action<bool> _callBackOnSwitchWeapon;
    [Header("Player Transform")]
    public Transform _transform;

    public Vector3 GetPlayerPosition() => _transform.position;
    [Header("Player Heart")]
    [SerializeField] private int _playerHeart;
    public Action<int> _callBackHeartDecress;

    public void DisableControl()
    {
        _fpsController.enabled = false;
        _machineGunController.enabled = false;
        _pistolGunController.enabled = false;
    }
    public void EnableControl()
    {
        _fpsController.enabled = true;
        _machineGunController.enabled = true;
        _pistolGunController.enabled = true;
    }
    public void SwitchBetweenWeapon()
    {
        _isUsingMachineGun = !_isUsingMachineGun;
        _machineGunArm.SetActive(_isUsingMachineGun);
        _pistolGunArm.SetActive(!_isUsingMachineGun);
    }
    private void Update()
    {
        if (_isChangeGun) return;
        if (Input.GetKeyDown(KeyCode.E))
        {
            _isUsingMachineGun = !_isUsingMachineGun;
            _callBackOnSwitchWeapon?.Invoke(_isUsingMachineGun);
            StartCoroutine(ChangeGunDelay(_isUsingMachineGun));
        }
    }
    private IEnumerator ChangeGunDelay(bool useMachineGun)
    {
        _isChangeGun = true;
        if (useMachineGun)
        {
            _pistolGunController.HolsteredWeapon(false);
            yield return new WaitForSeconds(_pistolGunHolsteredTime);
            _pistolGunArm.SetActive(false);
        }
        else
        {
            _machineGunController.HolsteredWeapon(false);
            yield return new WaitForSeconds(_machineGunHolsteredTime);
            _machineGunArm.SetActive(false);
        }

        if (!useMachineGun)
        {
            _pistolGunArm.SetActive(true);
            _pistolGunController.HolsteredWeapon(true);
            yield return new WaitForSeconds(_pistolGunHolsteredTime);
        }
        else
        {
            _machineGunArm.SetActive(true);
            _machineGunController.HolsteredWeapon(true);
            yield return new WaitForSeconds(_machineGunHolsteredTime);
        }
        _isChangeGun = false;
    }
    public int GetPlayerHeart() => _playerHeart;
    public void TakeDamage(int damage)
    {
        if (_playerHeart <= 0) return;
        _playerHeart -= damage;
        _callBackHeartDecress?.Invoke(_playerHeart);
        if (_playerHeart <= 0)
        {
            DebugClass.Log("End Game");
        }
    }
    public void SetMachineDamage(int damageHead, int damageBody)
    {
        _machineGunController.SetGunDamage(damageHead, damageBody);
    }
    public void SetPistolDamage(int damageHead, int damageBody)
    {
        _pistolGunController.SetGunDamage(damageHead, damageBody);
    }
}
