﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieEnhance : MonoBehaviour, IZombie
{
    private NavMeshAgent _agent;
    [Header("Zombie Setting")]
    [SerializeField] private float _distanceReachPlayer = 1f;
    [SerializeField] private float _distanceChase = 1.5f;
    [SerializeField] private float _distanceChaseWhenHearGunSound = 3f;
    [SerializeField] private float _distanceGoBackBase = 3f;
    private Vector3 _basePosition;
    private bool _triggerChase = false;
    private bool _triggerReturnBase = false;
    private bool _triggerKillingMode = false;
    private Transform _transform;
    private Animator _animator;
    [SerializeField] private int _damage;
    private ZombieState _currentState = ZombieState.Idle;
    [SerializeField] private int _zombieHeart = 100;
    private float _halfZombieHeart;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _transform = transform;
        _animator = GetComponent<Animator>();
        _basePosition = _transform.position;
        _halfZombieHeart = _zombieHeart / 2f;
    }
    private void OnEnable()
    {
        StopAllCoroutines();
    }
    private void Update()
    {
        if (_zombieHeart <= 0) return;
        var newState = CheckState();
        if (newState != _currentState)
        {
            _currentState = newState;
            switch (_currentState)
            {
                case ZombieState.Chase:
                    _animator.SetBool("Attack", false);
                    _animator.SetBool("Run", true);
                    break;
                case ZombieState.Attack:
                    _animator.SetBool("Run", false);
                    _animator.SetBool("Attack", true);
                    break;
                case ZombieState.Idle:
                    break;
                case ZombieState.Die:
                    _animator.SetTrigger("Die");
                    StartCoroutine(AutoTurnOff());
                    break;
                default:
                    break;
            }
        }
        if (_currentState == ZombieState.Chase)
        {
            if (_triggerKillingMode)
            {
                _agent.SetDestination(GameManager.Instance._playerController.GetPlayerPosition());
            }
            else if (_triggerReturnBase)
            {
                _agent.SetDestination(_basePosition);
            }
            else if (_triggerChase)
            {
                _agent.SetDestination(GameManager.Instance._playerController.GetPlayerPosition());
            }

        }
    }
    private ZombieState CheckState()
    {
        if (_zombieHeart <= 0) return ZombieState.Die;
        if (Utilities.CheckDistance(_transform.position, _basePosition, 0.1f))
        {
            _triggerReturnBase = false;
        }
        if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceReachPlayer))
        {
            return ZombieState.Attack;
        }
        else if (!_triggerKillingMode && Utilities.CheckDistance(_transform.position, _basePosition, _distanceGoBackBase, false))
        {
            _triggerReturnBase = true;
            return ZombieState.Chase;
        }
        else if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceChase))
        {
            _triggerChase = true;
            return ZombieState.Chase;
        }
        else if (_triggerChase)
        {
            return ZombieState.Chase;
        }
        else if (_triggerKillingMode)
        {
            _triggerReturnBase = false;
            return ZombieState.Chase;
        }
        return ZombieState.Idle;
    }
    private IEnumerator AutoTurnOff()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }

    public void TakeDamage(int damage)
    {
        if (_zombieHeart <= 0) return;
        _zombieHeart -= damage;
        DebugClass.Log("Zombie lose heart " + _zombieHeart, DebugKey.ZombieHeart);
        _currentState = ZombieState.Chase;
        if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceReachPlayer))
        {
            _currentState = ZombieState.Attack;
            _animator.SetBool("Run", false);
            _animator.SetBool("Attack", true);
        }
        else
        {
            _currentState = ZombieState.Chase;
            _triggerChase = true;
            _triggerReturnBase = false;
            _animator.SetBool("Attack", false);
            _animator.SetBool("Run", true);
            _agent.SetDestination(GameManager.Instance._playerController.GetPlayerPosition());
        }
        if (_zombieHeart <= _halfZombieHeart)
        {
            if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceChaseWhenHearGunSound))
            {
                if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceReachPlayer))
                {
                    _currentState = ZombieState.Attack;
                    _animator.SetBool("Run", false);
                    _animator.SetBool("Attack", true);
                }
                else
                {
                    _currentState = ZombieState.Chase;
                    _triggerKillingMode = true;
                    _animator.SetBool("Attack", false);
                    _animator.SetBool("Run", true);
                    _agent.SetDestination(GameManager.Instance._playerController.GetPlayerPosition());
                }
            }
        }
        if (_zombieHeart <= 0)
        {
            _animator.SetTrigger("Die");
            _currentState = ZombieState.Die;
            StartCoroutine(AutoTurnOff());
        }
    }
    public void MakeDamage()
    {
        if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceReachPlayer))
        {
            GameManager.Instance._playerController.TakeDamage(_damage);
        }

    }
    public void CheckGunSound()
    {
        if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceChaseWhenHearGunSound))
        {
            if (Utilities.CheckDistance(_transform.position, GameManager.Instance._playerController.GetPlayerPosition(), _distanceReachPlayer))
            {
                _currentState = ZombieState.Attack;
                _animator.SetBool("Run", false);
                _animator.SetBool("Attack", true);
            }
            else
            {
                _currentState = ZombieState.Chase;
                _triggerChase = true;
                _triggerReturnBase = false;
                _animator.SetBool("Attack", false);
                _animator.SetBool("Run", true);
                _agent.SetDestination(GameManager.Instance._playerController.GetPlayerPosition());
            }
        }
    }
}
