﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiePart : MonoBehaviour
{
    private IZombie _controller;
    private void Awake()
    {
        _controller = transform.root.GetComponent<ZombieController>();
    }
    public void TakeDamage(int damage)
    {
        _controller.TakeDamage(damage);
    }
}
