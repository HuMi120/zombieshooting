﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieManager : MonoBehaviour
{
    private List<IZombie> _listZombies = new List<IZombie>();
    public void CheckGunSoundAllZombies()
    {
        for (int i = 0; i < _listZombies.Count; i++)
        {
            _listZombies[i].CheckGunSound();
        }
    }
}
