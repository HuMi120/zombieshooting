﻿
using System.Collections.Generic;

public class StateMachine
{
    private Dictionary<int, IState> _allStates = new Dictionary<int, IState>();
    private IState _currentState;
    private StateID _currentStateExcute;
    private StateID _newStateExcute;

    public void Excute()
    {
        if (_newStateExcute != _currentStateExcute)
        {
            _currentState?.OnExit();
            _currentStateExcute = _newStateExcute;
            _allStates.TryGetValue((int)_currentStateExcute, out _currentState);
            _currentState.OnEnter();
        }
        _currentState.Excute();
    }
    public void SetStartState(StateID state)
    {
        _currentStateExcute = state;
        _newStateExcute = state;
        _allStates.TryGetValue((int)_currentStateExcute, out _currentState);
    }

    public void AddState(int id, IState state)
    {
        _allStates.Add(id, state);
    }
    public void SetState(StateID newState)
    {
        _newStateExcute = newState;
    }
}
