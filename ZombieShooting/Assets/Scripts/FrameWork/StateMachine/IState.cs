﻿public interface IState
{
    void Excute();
    void OnEnter();
    void OnExit();
}
