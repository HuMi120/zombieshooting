﻿public class Leaf : Node
{
	ISystem _system;

	public void SetFunction(ISystem system)
	{
		_system = system;
	}

	public override NodeState Run()
	{
		return _system.Run();
	}
}