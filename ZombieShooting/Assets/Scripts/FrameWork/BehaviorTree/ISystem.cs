﻿public interface ISystem
{
    NodeState Run();
}
