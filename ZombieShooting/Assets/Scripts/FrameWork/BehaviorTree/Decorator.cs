﻿
public abstract class Decorator : Node
{
    public Node child;
    /// Derivered classes will override this function to modify children node's state
    public abstract NodeState ProcessResult(NodeState childState);

    public override NodeState Run()
    {
        var childState = child.Run();
        return ProcessResult(childState);
    }
}

public class Inverter : Decorator
{
    public override NodeState ProcessResult(NodeState childState)
    {
        if (childState == NodeState.Success) return NodeState.Failure;
        if (childState == NodeState.Failure) return NodeState.Success;
        return NodeState.Running;
    }
}

public class Succeeder : Decorator
{
    public override NodeState ProcessResult(NodeState childState)
    {
        return NodeState.Success;
    }
}
public class RunUntilSuccess : Decorator
{
    public override NodeState ProcessResult(NodeState childState)
    {
        return childState;
    }
}