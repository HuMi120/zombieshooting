﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : SingletonOneScene<ObjectPool>
{
    #region Properties

    [System.Serializable]
    public class Pool
    {
        public ObjectKey _key;
        public GameObject _prefab;
    }

    Dictionary<ObjectKey, List<GameObject>> m_PoolingObjects = new Dictionary<ObjectKey, List<GameObject>>();
    [SerializeField] private List<Pool> _pools = null;

    #endregion

    #region Methods



    #region SpawnObject

    public GameObject SpawnObject(ObjectKey key, Transform parent = null)
    {
        if (!m_PoolingObjects.ContainsKey(key))
        {
            CreateNewObjectPoolingList(key, parent);
        }
        return CreateObject(key, parent);
    }
    #endregion


    #region CreateObject

    public GameObject CreateObject(ObjectKey key, Transform parent = null)
    {
        GameObject o = null;
        for (int i = 0, length = m_PoolingObjects[key].Count; i < length; i++)
        {
            o = m_PoolingObjects[key][i];
            if (!o.activeSelf) return o;
        }

        GameObject GamePrefab = Instantiate(o, parent);
        m_PoolingObjects[key].Add(GamePrefab);
        return GamePrefab;

    }
    #endregion
    #region CreateNewObjectPooling

    public void CreateNewObjectPoolingList(ObjectKey key, Transform parent)
    {
        List<GameObject> prefabs = new List<GameObject>();
        GameObject go = null;
        for (int i = 0, length = _pools.Count; i < length; i++)
        {
            if (_pools[i]._key == key) go = _pools[i]._prefab;
        }
        if (go == null)
        {
            DebugClass.LogError("No prefabs in list");
        }
        else
        {
            GameObject GamePrefab = Instantiate(go, parent);
            prefabs.Add(GamePrefab);
            m_PoolingObjects.Add(key, prefabs);
        }

    }
    #endregion


    #endregion

}