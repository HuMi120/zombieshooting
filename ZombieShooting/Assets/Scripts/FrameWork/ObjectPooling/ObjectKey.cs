﻿#region Game Object Key
public enum ObjectKey
{
    CHARACTER,
    BULLET,
    EXPLOSIVE,
    GRENADE,
    BIG_CASING,
    SMALL_CASING,
}
#endregion
